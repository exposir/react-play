import App from "./App";
import { StoreProvider } from "./contextStore";
import C from "./components/C";

const ContextApp = () => {
  return (
    <>
      <StoreProvider>
        <App />
      </StoreProvider>
      <C />
    </>
  );
};

export default ContextApp;

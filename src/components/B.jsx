import { observer } from "mobx-react";
import { store, todos } from "../store";

import { useContext } from "react";

import AppContext from "../AppContext";
import { useDispatch, useStore } from "../contextStore";

const addTodo = () => {
  console.log("render B addTodo");
  todos[0].title = "on B";
  console.log("todos", todos);
};

const subStore = () => {
  console.log("store", store);
  store.decrement();
};

const B = observer(() => {
  const context = useContext(AppContext) || {};
  console.log("B context", context);

  const { ncount } = useStore();
  const dispatch = useDispatch();

  console.log("render B");
  return (
    <div style={{ display: "flex", flexDirection: "column" }}>
      <button onClick={subStore}>B mobx {store.count}</button>
      <button onClick={addTodo}>B observer {todos[0].title}</button>
      <button
        onClick={() => {
          dispatch({ type: "decrement" });
        }}
      >
        B context {ncount}
      </button>
    </div>
  );
});

export default B;

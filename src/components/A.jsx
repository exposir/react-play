import { observer } from "mobx-react";
import { store, todos } from "../store";

import { useContext } from "react";
import AppContext from "../AppContext";

import { useDispatch, useStore } from "../contextStore";

const addTodo = () => {
  console.log("render A addTodo");
  todos[0].title = "on A";
  console.log("todos", todos);
};

const addStore = () => {
  console.log("store", store);
  store.increment();
};

const A = observer(() => {
  const context = useContext(AppContext) || {};
  console.log("A context", context);

  const { ncount } = useStore();
  const dispatch = useDispatch();

  console.log("render A");
  return (
    <div style={{ display: "flex", flexDirection: "column" }}>
      <button onClick={addStore}>A mobx {store.count}</button>
      <button onClick={addTodo}>A observer {todos[0].title}</button>
      <button
        onClick={() => {
          dispatch({ type: "increment" });
        }}
      >
        A context {ncount}
      </button>
    </div>
  );
});

export default A;

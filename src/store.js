import {
  makeAutoObservable, // makeAutoObservable 就像是加强版的 makeObservable，在默认情况下它将推断所有的属性。 与使用 makeObservable 相比，makeAutoObservable 函数更紧凑，也更容易维护，因为新成员不需要显式地提及。
  makeObservable, // 这个函数可以捕获已经存在的对象属性并且使得它们可观察。
  observable, //observable 注解可以作为一个函数进行调用，从而一次性将整个对象变成可观察的。原始值和类的实例永远不会被转化为可观察对象
  autorun,
  action,
  computed,
  runInAction,
} from "mobx";

// 1. make(Auto)Observable 仅支持已经定义的属性。请确保你的 编译器选项是正确的，或者，作为权宜之计，确保在你使用 make(Auto)Observable 之前已经为所有属性赋了值。如果没有正确的配置，已经声明而未初始化的字段（例如：class X { y; }）将无法被正确侦测到。
// 2. makeObservable 只能注解由其本身所在的类定义声明出来的属性。如果一个子类或超类引入了可观察字段，那么该子类或超类就必须自己为那些属性调用 makeObservable。

const store = makeObservable(
  // 需要代理的响应对象
  {
    count: 0,
    get double() {
      return this.count * 2;
    },
    increment() {
      runInAction(() => {
        this.count += 1;
      });
    },
    decrement() {
      this.count -= 1;
    },
  },
  // 对各个属性进行包装，用于标记该属性的作用
  {
    count: observable, // 需要跟踪的响应属性
    double: computed, // 计算属性
    increment: action, // action 调用后，会修改响应对象
    decrement: action, // action 调用后，会修改响应对象
  }
);

const todos = observable([
  { title: "Spoil tea", completed: true },
  { title: "Make coffee", completed: false },
]);

autorun(() => {
  console.log("Remaining:", todos[0]);
});

export { store, todos };

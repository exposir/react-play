import React from "react";
import ReactDOM from "react-dom/client";
import ContextApp from "./ContextApp";
import "./index.css";

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <ContextApp />
  </React.StrictMode>
);

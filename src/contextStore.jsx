import React from "react";

const initState = { ncount: 0 };

function reducer(state, action) {
  console.log("reducer", state, action);
  switch (action.type) {
    case "increment":
      return { ...state, ncount: state.ncount + 1 };
    case "decrement":
      return { ...state, ncount: state.ncount - 1 };
    default:
      return state;
  }
}

const StateContext = React.createContext();
const DispatchContext = React.createContext();

function useStore() {
  return React.useContext(StateContext);
}
function useDispatch() {
  return React.useContext(DispatchContext);
}

function StoreProvider({ children }) {
  const [state, dispatch] = React.useReducer(reducer, initState);

  return (
    <StateContext.Provider value={state}>
      <DispatchContext.Provider value={dispatch}>
        {children}
      </DispatchContext.Provider>
    </StateContext.Provider>
  );
}

export { useStore, useDispatch, StoreProvider };

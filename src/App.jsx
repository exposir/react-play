import reactLogo from "./assets/react.svg";
import mobxLogo from "./assets/mobx.png";
import "./App.css";
import A from "./components/A";
import B from "./components/B";

import { useState, useContext } from "react";

import AppContext from "./AppContext";

import { store, todos } from "./store";
import { observer } from "mobx-react";

import { useStore, useDispatch } from "./contextStore";

const App = observer(() => {
  const { ncount } = useStore();
  const dispatch = useDispatch();

  const context = useContext(AppContext) || {};
  console.log("APP context", context);

  console.log("render App");
  return (
    <div className="App">
      <div
        style={{
          display: "flex",
          justifyContent: "space-around",
          marginBottom: "20px",
        }}
      >
        <A />
        <B />
      </div>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
        }}
      >
        <button>App mobx {store.count}</button>
        <button>App observer {todos[0].title}</button>
        <button
          onClick={() => {
            dispatch({ type: "increment" });
          }}
        >
          App Context {ncount}
        </button>
      </div>
      <RenderElse />
    </div>
  );
});

const RenderElse = () => {
  const [count, setCount] = useState(0);
  return (
    <>
      <div>
        <img src="/vite.svg" className="logo" alt="Vite logo" />
        <img src={reactLogo} className="logo react" alt="React logo" />
        <img src={mobxLogo} className="logo" alt="Mobx logo" />
      </div>
      <h1>Vite React Store</h1>
      <div className="card">
        <button onClick={() => setCount((count) => count + 1)}>
          state count {count}
        </button>
      </div>
    </>
  );
};

export default App;
